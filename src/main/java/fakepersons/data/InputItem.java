package fakepersons.data;

public  class InputItem
{
    public InputItem(String firstName, String surname, String birthDate, String municipality, String street, String houseNumber, String idCardNumber, String idCardType)
    {
        this.firstName = firstName;
        this.surname = surname;
        this.birthDate = birthDate;
        this.municipality = municipality;
        this.street = street;
        this.houseNumber = houseNumber;
        this.idCardNumber = idCardNumber;
        this.idCardType = idCardType;
    }

    private String firstName;
    private String surname;
    private String birthDate;
    private String municipality;
    private String street;
    private String houseNumber;
    private String idCardNumber;
    private String idCardType;

    public String getFirstName()
    {
        return firstName;
    }

    public String getSurname()
    {
        return surname;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public String getMunicipality()
    {
        return municipality;
    }

    public String getStreet()
    {
        return street;
    }

    public String getHouseNumber()
    {
        return houseNumber;
    }

    public String getIdCardNumber()
    {
        return idCardNumber;
    }

    public String getIdCardType()
    {
        return idCardType;
    }
}
