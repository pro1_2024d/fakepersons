package fakepersons.customCheckers;

import fakepersons.checkers.OneByOneChecker;
import fakepersons.data.InputItem;

import java.util.HashSet;
import java.util.Set;

public class SuspiciousNameChecker extends OneByOneChecker
{
    @Override
    public String GetCheckerName()
    {
        return "Podezřelé jméno";
    }

    public static int UniqueCharCount(String string)
    {
        Set<Character> set = new HashSet<>();
        for (int i = 0; i<string.length();i++)
        {
            set.add(string.charAt(i));
        }
        return set.size();
    }

    public String CheckSingleItem(InputItem inputItem)
    {
        int count0 = UniqueCharCount(inputItem.getFirstName());
        int count1 = UniqueCharCount(inputItem.getSurname());

        if(count0 < inputItem.getFirstName().length() / 2)
        {
            return "Podezřelé křestní jméno";
        }

        if(count1 <  inputItem.getSurname().length() / 2)
        {
            return "Podezřelé příjmení";
        }
        return null;
    }

}
