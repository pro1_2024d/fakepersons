package fakepersons.customCheckers;

import fakepersons.checkers.Checker;
import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DuplicateChecker extends Checker
{
    @Override
    public OutputItem[] Check(InputItem[] input)
    {
        HashMap<String, InputItem> hashMap = new HashMap<>();
        List<OutputItem> result = new ArrayList<>();
       for (int i=0; i<input.length; i++)
       {
           String id = input[i].getIdCardNumber();
           if(id.isEmpty())
           {
               continue;
           }

           if(hashMap.containsKey(id))
           {
               InputItem old = hashMap.get(id);
               String message = "Stejné číslo dokladu jako "+
                       old.getFirstName()
                       + " " + old.getSurname();
               result.add(new OutputItem(i,message));
           }
           else
           {
               hashMap.put(id, input[i]);
           }
       }

       OutputItem[] resArray = new OutputItem[result.size()];
       result.toArray(resArray);
       return resArray;
    }

    @Override
    public String GetCheckerName()
    {
        return null;
    }
}
