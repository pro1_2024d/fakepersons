package fakepersons;

import fakepersons.checkers.Checker;
import fakepersons.customCheckers.DuplicateChecker;
import fakepersons.customCheckers.HouseNumberChecker;
import fakepersons.customCheckers.TooLongFirstNameChecker;
import fakepersons.customCheckers.MissingIdTypeChecker;
import fakepersons.data.InputItem;
import fakepersons.data.OutputItem;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        Properties config = new Properties();
        config.load(new FileInputStream("app.config"));
        InputItem[] input = Utils.ReadInputFile();

        List<Checker> checkers = Arrays.asList(
                new HouseNumberChecker()
        );

        for(Checker checker : checkers)
        {
            checker.CheckAndPublish(input, config);
        }
    }
}
